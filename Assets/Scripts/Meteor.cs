﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Enemy 
{
    private float rotationDegrees;
    private float rotationDirection;
    private Animation animation;
    private TrailRenderer trailRenderer;

    public float rotationSpeed;
    public float minDegrees;
    public float maxDegrees;

    void Start()
    {
        rotationDegrees = Random.Range(minDegrees, maxDegrees);
        rotationDirection = Random.Range(-1, 1);
        animation = GetComponent<Animation>();
        trailRenderer = GetComponent<TrailRenderer>();

        if (rotationDirection < 0)
            rotationDirection = -1;
        if (rotationDirection >= 0)
            rotationDirection = 1;
    }

	void Update () 
    {
        if (isDying)
            return;
        
        step = Enemy.Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        transform.Rotate(0, 0, rotationDegrees * Time.deltaTime * rotationSpeed * rotationDirection);
	}

    public override void OnDeath()
    {
        isDying = true;
        trailRenderer.Clear();
        StartCoroutine(Animate(animation, deathAnimationName));
        Debug.Log("hi");
        //gameObject.SetActive(false);
    }

    public override void OnInvading()
    {
        isDying = true;
        StartCoroutine(Animate(animation, deathAnimationName));
        gameObject.SetActive(false);
    }
}
