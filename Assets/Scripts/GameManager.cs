﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
    private Planet planet;
    private Enemy[] enemies;
    private EnemyManager enemyManager;
    private MenuManager menuManager;
    private Rocket rocket;

	void Start () 
    {
        planet = FindObjectOfType<Planet>();
        enemyManager = FindObjectOfType<EnemyManager>();
        menuManager = FindObjectOfType<MenuManager>();
        rocket = FindObjectOfType<Rocket>();
	}

    public void StartGame()
    {
        InitializeGame();
    }

    public void RestartGame()
    {
        InitializeGame();
    }

    public void GameOver()
    {
        EndGame();
        menuManager.OnDeathMenu();
    }

    private void EndGame()
    {
        enemyManager.StopSpawning();
        this.enemies = FindObjectsOfType<Enemy>();
        Enemy.Speed = Enemy.SpeedInitial;
        planet.Ammo = planet.initialAmmo;
        rocket.GasPointerUp();

        foreach (var enemy in enemies)  
        {
            enemy.gameObject.SetActive(false);
        }
    }

    private void InitializeGame()
    {
        planet.Health = planet.initialHealth;
        planet.Ammo = planet.initialAmmo;
        planet.Kills = 0;

        if (!enemyManager.IsSpawning)
            enemyManager.StartSpawning();
    }
}
