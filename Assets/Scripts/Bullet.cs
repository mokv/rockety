﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Planet planet;
    public float speed;

	void Update ()
    {
        transform.position += transform.up * Time.deltaTime * speed;
	}

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Enemy>().OnDeath();
            this.gameObject.SetActive(false);
            planet.Kills++;
        }

        if (other.gameObject.tag == "Fence")
        {
            this.gameObject.SetActive(false);
        }
    }

    void OnBecameInvisible()
    {
        this.gameObject.SetActive(false);
    }
}
