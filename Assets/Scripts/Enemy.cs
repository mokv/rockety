﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour 
{
    protected string deathAnimationName = "EnemyDeath";
    protected float step;
    protected bool isDying;

    public Transform target;
    public static float Speed;
    public static float SpeedIncreaser = 0.4f;
    public static float SpeedInitial;

    void Start()
    {
        isDying = false;
    }

    void Awake()
    {
        isDying = false;
    }

    void OnEnable()
    {
        isDying = false;
    }

    public abstract void OnDeath();

    public abstract void OnInvading();

    protected virtual IEnumerator Animate(Animation animation, string name)
    {
        animation.Play();
        yield return new WaitForSecondsRealtime(animation.clip.length);
    }
}
