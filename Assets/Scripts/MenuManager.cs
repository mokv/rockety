﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    private GameManager gameManager;

    public GameObject inGameCanvas;
    public GameObject deathMenuCanvas;
    public GameObject mainMenuCanvas;

	void Start () 
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void StartPlaying()
    {
        deathMenuCanvas.SetActive(false);
        mainMenuCanvas.SetActive(false);
        gameManager.StartGame();
        inGameCanvas.SetActive(true);
    }

    public void RestartGame()
    {
        deathMenuCanvas.SetActive(false);
        mainMenuCanvas.SetActive(false);
        gameManager.RestartGame();
        inGameCanvas.SetActive(true);
    }

    public void OnDeathMenu()
    {
        inGameCanvas.SetActive(false);
        mainMenuCanvas.SetActive(false);
        deathMenuCanvas.SetActive(true);
    }

    public void BackToMainMenu()
    {
        inGameCanvas.SetActive(false);
        deathMenuCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
    }
}
