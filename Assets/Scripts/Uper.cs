﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uper : Enemy {

    private Vector3 direction;
    private float angle;
    private Rigidbody2D rigidbody;
    private bool hasDowned;
    private Animation animation;

    public float force;
    public float distance;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animation = GetComponent<Animation>();
        hasDowned = false;
    }

    void OnEnable()
    {
        hasDowned = false;
    }

    void Update () 
    {
        if (isDying)
            return;
        
        if (!hasDowned)
        {
            if (transform.position.x < distance && transform.position.x > -distance)
            {
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, force);
                hasDowned = true;
            }
        }

        step = Enemy.Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        direction = target.transform.position - transform.position;
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle + 90f, Vector3.forward);
    }

    public override void OnDeath()
    {
        isDying = true;
        StartCoroutine(Animate(animation, deathAnimationName));
        gameObject.SetActive(false);
    }

    public override void OnInvading()
    {
        isDying = true;
        StartCoroutine(Animate(animation, deathAnimationName));
        gameObject.SetActive(false);
    }
}
