﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

	public Transform[] spawnPoints;
	public float spawnTime;
    public GameObject[] enemies;
    public int pooledAmount;
    public float enemySpeed;

    private ObjectPooler[] objPoolers;
	private int spawnPointsIndex;
    private int randomEnemyIndex;
    private string spawningMethodName = "Spawn";

    public bool IsSpawning 
    { 
        get { return IsInvoking(spawningMethodName); }
    }

	void Start () 
    {
        Enemy.SpeedInitial = enemySpeed;
        Enemy.Speed = enemySpeed;
        objPoolers = new ObjectPooler[enemies.Length];

        for (int i = 0; i < enemies.Length; i++)
        {
            objPoolers[i] = ScriptableObject.CreateInstance<ObjectPooler>();
            objPoolers[i].Initialize(enemies[i], pooledAmount);
        }
	}

	void Spawn()
	{
		spawnPointsIndex = Random.Range (0, spawnPoints.Length);
        randomEnemyIndex = Random.Range(0, enemies.Length);
        GameObject enemy = objPoolers[randomEnemyIndex].GetPooledObject ();
		enemy.transform.position = spawnPoints [spawnPointsIndex].position;
		enemy.transform.rotation = spawnPoints [spawnPointsIndex].rotation;
		enemy.SetActive (true);
	}

    public void StartSpawning()
    {
        InvokeRepeating (spawningMethodName, spawnTime, spawnTime);
    }

    public void StopSpawning()
    {
        CancelInvoke(spawningMethodName);
    }
}