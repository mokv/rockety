﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : ScriptableObject
{
    private int pooledAmount;
    private GameObject pooledObject;
    private List<GameObject> objectPool;

    void Awake()
    {
        this.objectPool = new List<GameObject>();
    }

    public void Initialize(GameObject pooledObjectInit, int pooledAmountInit)
    {
        this.pooledAmount = pooledAmountInit;
        this.pooledObject = pooledObjectInit;

        for (int i = 0; i < pooledAmount; i++) 
        {
            CreateObject ();
        }
    }

    public GameObject GetPooledObject ()
	{
        for (int i = 0; i < objectPool.Count; i++) 
		{
            if (!objectPool [i].activeInHierarchy) 
			{
                return objectPool[i];
			}
		}

        return CreateObject ();
	}

	private GameObject CreateObject()
	{
        GameObject obj = (GameObject)Instantiate (pooledObject);
		obj.SetActive (false);
        objectPool.Add (obj);
		return obj;
	}
}
