﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour 
{
    private int health;
    private int kills;
    private int ammo;
    private GameManager gameManager;

    public int coins;
    public int initialHealth;
    public int initialAmmo;
    public int enemyDamage;
    public Text killsText;
    public Text healthText;
    public Text ammoText;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        health = initialHealth;
        ammo = initialAmmo;
        ammoText.text = ammo.ToString();
        killsText.text = kills.ToString();
        healthText.text = health.ToString();
    }

    public int Ammo
    {
        get { return ammo; }
        set 
        { 
            ammo = value; 
            ammoText.text = ammo.ToString();
        }
    }

    public int Health
    {
        get { return health; }
        set 
        { 
            health = value; 
            healthText.text = health.ToString();

            if (this.health <= 0)
            {
                gameManager.GameOver();
            }
        }
    }

    public int Kills
    {
        get { return kills; }
        set 
        { 
            kills = value; 
            killsText.text = kills.ToString();
        }
    }

    void OnTriggerEnter2D( Collider2D other )
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Enemy>().OnInvading();
            Health -= enemyDamage;
        }
    }
}
