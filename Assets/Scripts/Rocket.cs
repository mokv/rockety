﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour 
{
    private float movementCounter = 0;
    private float timeWithSpeed;
    private float zRotation;
    private float xPosition = 0;
    private float yPosition = 0;
    private float speedMemory; 
    private float circleInRadians;
    private Planet planet;
    private ObjectPooler objPooler;
    private bool buttonPressed;

	public float speed;
	public float range;
	public float speedIncrease;
    public GameObject bullet;
    public int bulletsPooledAmount;

	void Start () 
    {
		speedMemory = speed;
		circleInRadians = 360 / (2 * Mathf.PI);
        planet = FindObjectOfType<Planet>();
        objPooler = ScriptableObject.CreateInstance<ObjectPooler>();
        objPooler.Initialize(bullet, bulletsPooledAmount);
        buttonPressed = false;
	}

	void Update () 
    {
        timeWithSpeed = Time.deltaTime * speed;
        movementCounter += timeWithSpeed;
        xPosition = Mathf.Cos(movementCounter) * range;
        yPosition = Mathf.Sin(movementCounter) * range;
        zRotation += timeWithSpeed * circleInRadians;
        transform.position = new Vector2(xPosition, yPosition);
        transform.eulerAngles = new Vector3(0, 0, zRotation);

        if (Input.GetMouseButtonDown(0))
        {
            if (!buttonPressed)
            {
                if (planet.Ammo > 0)
                {
                    GameObject bullet = objPooler.GetPooledObject();
                    bullet.transform.position = transform.position;
                    Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    Vector3 difference = target - transform.position;
                    difference.Normalize();
                    float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                    bullet.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ - 90);
                    Physics2D.IgnoreCollision(bullet.GetComponent<PolygonCollider2D>(), planet.GetComponent<CircleCollider2D>());
                    bullet.SetActive(true);
                    planet.Ammo--;
                    Enemy.Speed -= Enemy.SpeedIncreaser;
                }
            }

            buttonPressed = false;
        }
	}

	public void GasPointerDown()
	{
        buttonPressed = true;
		speedMemory = speed;
		speed += speedIncrease;
	}

	public void GasPointerUp()
	{
        buttonPressed = false;
		speed = speedMemory;
	}

    void OnTriggerEnter2D ( Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Enemy>().OnDeath();
            planet.Kills++;

            if (ShouldGenerateAmmo())
            {
                planet.Ammo++;
                Enemy.Speed += Enemy.SpeedIncreaser;
            }
        }
    }

    private bool ShouldGenerateAmmo()
    {
        int randomNumber = Random.Range(0, 2);

        if (randomNumber == 0)
        {
            return true;
        }

        return false;
    }
}